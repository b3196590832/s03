const names = {
	"Brandon": {
		"name": "Brandon Boyd",
		"alias": "Bran",
		"age": 35
	},
	
	"Steve": {
		"name": "Steve Aoki",
		"alias": "Stev",
		"age": 99
	}
};

const usernames = {
	"Brandon": {
		"username": "Incubus",
		"age": 35

	},
	
	"Steve": {
		"username": "LinkinPark",
		"age": 99
	}

};

module.exports = {
    names: names,
    usernames: usernames
};

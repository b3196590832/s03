const chai = require ("chai");
const expect = chai.expect;


const http = require("chai-http");
chai.use(http);


describe("api_test_suite", () => {

	it("test_api_get_people_is_running", () => {
		chai.request("http://localhost:5001")
			.get("/people")
			.end((err, res) => {
				expect(res).to.not.equal(undefined);
		})
	})


	it("test_api_get_people_returns_200", (done) => {
		chai.request("http://localhost:5001")
			.get("/people")
			.end((err, res) => {
				expect(res.status).to.equal(200)
				done()
		})
	})

	it("test_api_post_person_returns_400_if_no_person_name", (done) => {
		chai.request("http://localhost:5001")
		.post("/person")
		.type("json")
		.send({
			alias: "james",
			age: 28
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})

	})


	it("test_api_post_person_returns_400_if_age_is_string", (done) => {
		chai.request("http://localhost:5001")
		.post("/person")
		.type("json")
		.send({
			alias: "james",
			age: "twenty"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})

	})



	it("test_api_post_person_returns_400_if_NO_ALIAS", (done) => {
		chai.request("http://localhost:5001")
		.post("/person")
		.type("json")
		.send({
			alias: "james",
			age: 20
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})

	})

		it("test_api_post_person_returns_400_if_NO_AGE", (done) => {
		chai.request("http://localhost:5001")
		.post("/person")
		.type("json")
		.send({
			alias: "james",
			age: 20
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})

	})

})

describe("api_test_suite_activity", () => {

	it("test_api_get_username_is_running", () => {
		chai.request("http://localhost:5001")
			.get("/users")
			.end((err, res) => {
				expect(res).to.not.equal(undefined);
		})
	})


	it("test_api_get_users_returns_200", (done) => {
		chai.request("http://localhost:5001")
			.get("/users")
			.end((err, res) => {
				expect(res.status).to.equal(200)
				done()
		})
	})

	it("test_api_post_users_returns_400_if_no_username", (done) => {
		chai.request("http://localhost:5001")
		.post("/users")
		.type("json")
		.send({
			username: "LinkinPark",
			age: 28
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})

	})

	it("test_api_post_users_returns_400_if_no_age", (done) => {
		chai.request("http://localhost:5001")
		.post("/users")
		.type("json")
		.send({
			username: "LinkinPark",
			age: 28
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done()
		})

	})

})